// Class for COMP2931 Coursework 1

package comp2931.cwk1;



import java.util.Arrays;
import java.util.List;
import java.util.Calendar;


/**
 * Simple representation of a date.
 */
public class Date {

  //Class constants (shared by all Date objects)
  private static final int MONTHS = 12;
  private static final int FEB = 28;
  private static final int FEBLEAP = 29;
  private static final int THIRTY = 30;
  private static final int THIRTYONE = 31;

  //lists to hold the date values for a given month
  List<Integer> ListThirty = Arrays.asList(4, 6, 9, 11);
  List<Integer> ListThirtyOne = Arrays.asList(1, 3, 5, 7, 8, 10, 12);

  private int year;
  private int month;
  private int day;

  /**
   * Constructor creating today's date
   *
   */
  public Date() {

    Calendar cal = Calendar.getInstance();
    int y = cal.get(Calendar.YEAR);
    int m = cal.get(Calendar.MONTH);
    int d = cal.get(Calendar.DAY_OF_MONTH);

    year = y;
    month = m+1;
    day = d;
  }
  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    set(y, m, d);
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }


  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }


  /**
   * Provides an int which represents the day of the year a date is
   *
   * @return int
   */
  public int getDayofYear() {
      int day;

      Calendar cal = Calendar.getInstance();
      //sets the day/month/year of cal with ints from date object using 'this'
      cal.set(Calendar.YEAR, this.getYear());
      //minus 1 as month value is zero based eg 0 for January
      cal.set(Calendar.MONTH, this.getMonth()-1);
      cal.set(Calendar.DAY_OF_MONTH, this.getDay());

      day = cal.get(Calendar.DAY_OF_YEAR);

      return day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
      return String.format("%04d-%02d-%02d", year, month, day);
  }

  /**
   *Checks whether two date objects are the same and the .toString objects are the same
   *
   * @param date Date object with which to compare to
   * @return true if date is equal to this date, false if not
   */
  @Override
  public boolean equals(Object date) {

      Date otherdate = (Date) date;
      //checks if the date objects are the same
      if (this == date) {
          return true;
      }
      //checks if the string representation of date objects are the same
      else if (otherdate.year == this.year &&
               otherdate.month == this.month &&
               otherdate.day == this.day) {
          return true;
      }
      //return false if neither of the above are true
      else {
          return false;
      }

  }

  /**
   * Check the dates are acceptable (correct number of days per month etc)
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  private void set(int y, int m, int d) {

      if (m < 1 || m > MONTHS) {
          throw new IllegalArgumentException("Months invalid(too high or too low)");
      }
      else if (d < 1) {
          throw new IllegalArgumentException("Day invalid(too low)");
      }
      else if (m == 2 && y%4 != 0 && d > FEB) {
          throw new IllegalArgumentException("Day invalid(too high)");
      }
      else if (m == 2 && y%4 == 0 && d > FEBLEAP) {
          throw new IllegalArgumentException("Day invalid(too high)");
      }
      else if (ListThirty.contains(m) && d > THIRTY ) {
          throw new IllegalArgumentException("Day invalid(too high)");
      }
      else if (ListThirtyOne.contains(m) && d > THIRTYONE ) {
          throw new IllegalArgumentException("Day invalid(too high)");
      }
      else {
          year = y;
          month = m;
          day = d;
      }
  }

  /**
   * Main class to test the outputs of some variables
   *
   * @param args no args supplied
   */
  public static void main(String args[]) {
      Date nYday = new Date(2000,1,1);
      Date nYeve = new Date(1999, 12,31);
      Date bDay = new Date(1984,6,9);
      Date today = new Date();


      System.out.println(today.toString());
      System.out.println(nYeve.toString());
      System.out.println(bDay.toString());
      System.out.println(bDay.getDayofYear());
      //System.out.println(nYday.getDayofYear());
      //System.out.println(nYeve.getDayofYear());
  }
}

