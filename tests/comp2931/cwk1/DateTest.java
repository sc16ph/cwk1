package comp2931.cwk1;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

public class DateTest {

    //constants used by tests
    private static Date myBday;
    private static Date millenium;
    private static Date wSneijder;
    private static Date mRashford;
    private static Date nYearseve;

    //set up of constants
    @BeforeClass
    public static void setUp() {
        myBday = new Date (1984,6,9);
        nYearseve = new Date(1999,12,31);
        millenium = new Date(2000,1,1);
        wSneijder = new Date(1984, 6, 9);
        mRashford = new Date(1997, 10, 31);

    }

    //testing that the toSting method is working correctly
    @Test
    public void datestring() throws Exception {

        assertThat(millenium.toString(), is("2000-01-01"));
        assertThat(myBday.toString(), is("1984-06-09"));
    }

    //testing the equals method. for date objects and the string representation
    //of those objects
    @Test
    public void compare() throws Exception {

        assertThat(myBday.equals(wSneijder), is(true));
        assertThat(myBday.equals(mRashford), is(false));

        String mBdystr = myBday.toString();
        String wSstr = wSneijder.toString();
        String mRstr = mRashford.toString();

        assertThat(mBdystr.equals(wSstr), is(true));
        assertThat(mBdystr.equals(mRstr), is(false));
    }

    //test to check that the day of the year is correct for the date supplied
    @Test
    public void dayOfYear() {
        //these three dates are leap years
        assertThat(myBday.getDayofYear(), is(161));
        assertThat(wSneijder.getDayofYear(), is(161));
        assertThat(millenium.getDayofYear(), is(1));
        //these two dates are not leap years
        assertThat(mRashford.getDayofYear(), is(304));
        assertThat(nYearseve.getDayofYear(), is(365));

    }

    //testing that date objects don't accept a month above 12
    @Test(expected=IllegalArgumentException.class)
    public void monthtoohigh() {
        new Date(2017, 13, 22);
    }

    //testing that date objects don't accept a month below 1
    @Test(expected=IllegalArgumentException.class)
    public void monthtoolow() {
        new Date(2017, 0, 4);
    }

    @Test(expected=IllegalArgumentException.class)
    public void daytoolow() {
        new Date(2017, 5, 0);
    }

    @Test(expected=IllegalArgumentException.class)
    public void thirtydaytohigh() { new Date(2027, 4, 31); }

    @Test(expected=IllegalArgumentException.class)
    public void thirtyonedaytohigh() { new Date(2028, 10, 32); }

    @Test(expected=IllegalArgumentException.class)
    public void febdaytohigh() { new Date(2017, 2, 29); }
}